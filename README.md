
<!-- README.md is generated from README.Rmd. Please edit that file -->

# opendataphilly <img src="man/figures/opendataphilly.png" align="right" alt="" width="120" />

-----

<!-- badges: start -->

[![Lifecycle:
experimental](https://img.shields.io/badge/lifecycle-experimental-orange.svg)](https://www.tidyverse.org/lifecycle/#experimental)
[![Netlify
Status](https://api.netlify.com/api/v1/badges/1879a43e-0b24-433a-99e9-85f09e479055/deploy-status)](https://app.netlify.com/sites/opendataphilly/deploys)
<!-- badges: end -->

This is an UNOFFICIAL package that provides functions to facilitate the
finding and retrieval of datasets from <https://opendataphilly.org>.

## Installation

``` r
# Install development version from GitLab

devtools::install_gitlab("db369/opendataphilly")
```

## Usage

-----

### List datasets

``` r
odp_get_package_list()
```

This will download all datasets (or packages) available through the
opendataphilly api. Data is provided in a number of formats, but only
CSV is supported by this package right now. To see what formats are
available for each dataset, you can try the following:

``` r
odp_get_package_list(format=TRUE)
```

This may be slow so you may find filtering the list a little faster. For
example:

``` r
odp_get_package_list('crime', format=TRUE)
```

### Dataset description

Once you find a dataset to explore, you retrieve a description of the
data with `odp_pkg_describe()`, which requires an index or package name.

``` r
odp_pkg_describe(418)
  or
odp_pkg_describe('ppr-spraygrounds')
```

### Data dictionary (or field descriptions)

To get a summary of the data fields, the `odp_get_metadata()` will be
helpful.

``` r
odp_get_metadata(418)
  or
odp_get_metadata('ppr-spraygrounds')
```

### Download data

And `odp_get_pkg_data()` will download the dataset to dataframe,
assuming a CSV format exists. Support for other formats (like shape
files and GeoJSON) may come.

``` r
odp_get_pkg_data(418)
  or
odp_get_pkg_data('ppr-spraygrounds')
```

### Recently updated data

`odp_get_recent_chgs()` is useful for identifying datasets that have
recently changed.

``` r
odp_get_recent_chgs()
```
